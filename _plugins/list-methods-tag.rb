module Jekyll
  class ListMethodsTag < Liquid::Tag

    def render(context)

      methods = ""

      methods << "Method, Gem, Version, CodeUrl, DocUrl, \n"

      liquidMethods = Liquid::StandardFilters.instance_methods(false)
      liquidMethods.sort.each do |m|
        methods << m.to_s + ", Liquid,  #{Liquid::VERSION}, \n"
      end

      jekyllMethods = Jekyll::Filters.instance_methods(false)
      jekyllMethods.sort.each do |m|
        methods << m.to_s + ", Jekyll,  #{Jekyll::VERSION}, \n"
      end

      methods

    end
  end
end

Liquid::Template.register_tag("methods", Jekyll::ListMethodsTag)
